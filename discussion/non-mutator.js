// Non-Mutator Methods
/*
- Non-mutator methods are functions that do not modify or change an array after they have been created
- These methods do not manipulate the original array
- These methods perform tasks such as returning (or getting) elements from an array, combining arrays, and printing the output
- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()

*/
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);

// indexOf()
/*
- Returns the index number of the "first matching element" found in an array
- if no matcch has been found, the result will be -1
- The search process will be done from the first element proceeding to the last element.
- Syntax:
	arrayName.indexOf(searchValue);
	arrayName.indexOf(searhValue, fromIndex);
*/

console.log('indexOf Method: ');
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf Method: ' + invalidCountry);

// lastIndexOf()
/*
- Returns the index number of the "last matching item" found in the array
- The search process will be done from the last element proceeding to the first element.
- Syntax:
	arrayName.lastIndexOf(seachValue);
	arrayName.lastIndexOf(searchValue, fromIndex);
*/

// Getting the index number starting from the last element
console.log('lastIndexOf Method: ');
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

// Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
let lastIndexStarter = countries.lastIndexOf('PH', 4);
console.log('Result of lastIndexOf method: ' + lastIndexStart);
console.log('Result of lastIndexOf method: ' + lastIndexStarter);

// slice()
/*
- Slices elements from an array AND returns a new array
- Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, endingIndex);
*/
console.log('slice Method ');

// Slicing Off elements from a specified index to the last element
let slicedArrayA = countries.slice(2);
console.log('Return from slice method:');
console.log(slicedArrayA);

// Slicing off elements from a specified index to another index
let slicedArrayB = countries.slice(2, 4);
console.log("Return from slice method:")
console.log(slicedArrayB);

// Slicing off elements starting from the last element of an array
let slicedArrayC = countries.slice(-1);
let slicedArrayD = countries.slice(-3);
let slicedArrayE = countries.slice(-5);
console.log("Result from slice method:");
console.log(slicedArrayC);
console.log(slicedArrayD);
console.log(slicedArrayE);

// toString()
/*
- Returns an array as a string separated by commas
- Syntax:
	arrayName.toString();
*/
console.log('toString Method ');

let stringArray = countries.toString();
console.log("Result from toString method:");
console.log(stringArray);

// concat()
/*
- Combines two arrays and returns the combined result
- Syntax:
	arrayA.concat(arrayB);
	arrayA.concat(elementA, elementB);
*/

console.log('concat Method ');
let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ["inhale CSS", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method:");
console.log(tasks);

// Combining multiple Arrays
console.log("Result from concat method:");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// Combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log("Result from concat method:");
console.log(combinedTasks);


// join()
/*
- Returns an array as a string separated by specific separator string
- Syntax:
	arrayName.join("separatorString");
*/
console.log('join Method ');

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(""));
console.log(users.join('  -  '));
console.log(users.join('    '));

// Iteration Methods
/*
- Iteration methods are loops designed to perform repetitive tasks on arrays
- Useful for manipulating array data resulting in complex tasks.
*/


// foreach
/*
- Similar to the for loop that iterates on each array element
- Variable names for arrays are usually written in the plural form of the data stored in an array
- It is a common practice to use the singular form of the array
- Syntax:
	arrayName.forEach(function(individualElement) {statement})
*/

allTasks.forEach(function(task){
	console.log(task);
});

let students = ['Jeru', 'Anne', 'Glyn', 'Jake', 'Gabryl', 'Daniel'];

students.forEach(function(student){
	console.log("Hi " + student + "!");
});

// Using forEach with conditional statements
console.log("Filtered forEach");

let filteredTasks = [];

allTasks.forEach(function(task){
	if (task.length > 10) {
		console.log(task)
		filteredTasks.push(task);
	} 
});

console.log("Result of filtered tasks:");
console.log(filteredTasks);

// map()
/*
- It iterates of each element AND returns new array with different values depending on the result of the function's operation
- this is useful for performing tasks where mutating/changing the elements are required
- Unlike the forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation
- Syntax:
	let / const resultArray = arrayName.map(function(individualElement))
*/

let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number) {
	return number * number
})

console.log("Result of map method:");
console.log(numberMap);


// every()
/*
- Checks if "all" element in an array meet the given condition
- This is useful for validating data stored in arrays especially when dealing with large amounts of data.
- Returns the true value if all elements meet the condition and false if otherwise
- Syntax:
	let / const resultArray = arrayName.every(function(element){
		return expression / condition
	})
*/
console.log('every() method');
let allValid = numbers.every(function(number) {
	return (number < 3);
});
console.log("Result of every method:");
console.log(allValid);


// some()
/*
- Checks if "atleast one" element in the array meets the given condition
- Returns a true value if atleast one element meets the condition and false if otherwise
- Syntax:
	let / const resultArray = arrayName.some(function(element) {
		return expression / condition
	})
*/
console.log('some() method');

let someValid = numbers.some(function(number) {
	return (number < 2);
})

console.log("Result of some method:");
console.log(someValid);

// Combining the returned result from the every/some method may be used in other statements to perform consecutive
if (someValid) {
	console.log("Some numbers in the array are greater than 2");
}


// filter()
/*
- Returns new array that contains elements which meets the given condition
- Returns an empty array if no elements are found
- Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods.
- Syntax:
	let / const resultArray = arrayName.filter(function(element) {
		return expression / condition
	});
*/

console.log('filter() method');

let filterValid = numbers.filter(function(number) {
	return (number < 3);
})
console.log("Result of the filter method:");
console.log(filterValid);

let nothingFound = numbers.filter(function(number) {
	return (number = 0);
})
console.log(nothingFound);

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number) {
	console.log(number)
	if (number < 3) {
		filteredNumbers.push(number)
	}
	
});
console.log("Result of filter method:")
console.log(filteredNumbers);


// includes() method
/*
- Methods can be "chained" by using them one after another
- The result of the first method is used on the second method until all chained methods have been solved.
*/
console.log('includes() method');

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a");
});

console.log(filteredProducts);


// reduce()
/*
- Evaluates statements from left to right and returns / reduces the array into a single value.
- Syntax:
	let / const resultArr = arrayName.reduce(function(accumulator, currentValue) {
		return expression / condition
	});
- The "accumulator" parameter in the function stores the result for every iteration of the loop
- The "currentValue" is the current / next element in the array that is evaluated in each iteration of the loop.
	How the "reduce" method works:
		1. The first/result element in the array is stores in the "accumulator"
		2. The second/next element in the array is stores in the "currentValue"
		3. An operation is performed on the two elements
		4. The loop repeats steps 1 to 3 until all elements have been worked on.
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y) {
	console.log('current iteration: ' + ++iteration);
	console.log('accumulator: ' + x)
	console.log('currentValue: ' + y)
	return x + y
})

console.log('Result of reduce method: ' + reducedArray);


// Reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
	console.log('accumulator: ' + x)
	console.log('currentValue: ' + y)
	return x + ' ' + y;
})

console.log("Result of reduce method: " + reducedJoin);