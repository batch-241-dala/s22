// Array Methods
// For Array Methods, we have the mutator and non-mutator methods

// Mutator Methods

/*
- Mutator methods are functions that "mutate" or change an array after they're created.
- These methods manipulate the original array. Performing tasks such as adding and removing elements.
-push(), pop(), unshift(), shift(), splice(), sort(), & reverse()
*/
console.log('Mutator Methods');
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log(fruits);

// push()
/*
- "Adds" in the "end" of the array AND returns the array's length
-[0, 0, "X"];
-Syntax:
	arrayName.push()
*/
console.log('Push Method:')
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array from push method: ");
console.log(fruits);

// Push multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method: (multiple values) ');
console.log(fruits);

// pop()
/*
- "Removes" the last element in an array AND returns the removed element
- [0, 0, "X"]
- Syntax:
	arrayName.pop();
*/

console.log('Push Method:')
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);


// unshift()
/*
- "Adds" at the "start" of an array
-["X", 0, 0]
- Syntax:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB');
*/

console.log('Unshift Method:')
fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method");
console.log(fruits)

// shift()
/*
- Removes element at the "start" of an array AND returns the removed element
- Syntax:
	arrayName.shift();
*/
console.log('Shift Method:')
let anotherFruit = fruits.shift();
console.log(anotherFruit)
console.log("Mutated array from shift method: ");
console.log(fruits)

// splice()
/*
- Simultaneously removes element from a specified number and adds elements
- Syntax:
	arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)

	arrayName.splice(whereToStartDeleting, NumberOfItemsToBeDeleted, elementsToBeAdded)
*/
console.log('Splice Method:')
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Mutated array from splice method: ")
console.log(fruits);

// sort()
/*
- Rearranges the array elements in an Alphanumeric Order
- Syntax:
	arrayName.sort()
*/

console.log('Sort Method: ')
fruits.sort();
console.log('Mutated array from sort method: ')
console.log(fruits);

// reverse()
/*
- Reverses the order of array elements
- Syntax:
	arrayName.reverse()
*/
console.log('Reverse Method: ')
fruits.reverse();
console.log('Mutated array from reverse method:');
console.log(fruits)
